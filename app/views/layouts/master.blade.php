<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>@yield('title')</title>
		
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" type="text/css" href="/2503ict-assign1/public/main.css">
		
		<script>
			function validateForm()
			{
				var form = document.forms[0];
				for (var i=0; i<form.length; i++)
				{
					if (form[i].value.trim() == '' || form[i].value.trim() == null)
					{
						document.getElementById("submit").className = "button";
						return false;
					}
				}
				document.getElementById("submit").className = "button clickable";
				return true;
			}
			
			function submitForm()
			{
				if (validateForm())
				{
					document.forms[0].submit(); 
				}
				return false;
			}
		</script>
	</head>
	<body>
		<nav>
			<span class="logo">
				<a href="/2503ict-assign1/public/">Generic Social Network</a>
			</span>
			<span class="nav">
			    <a href="/2503ict-assign1/public/docs">Documentation</a>
			</span>
		</nav>
		<main>
            @section('content')
            There is no content on this page.
            @show
		</main>
	</body>
</html>
