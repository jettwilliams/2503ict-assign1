@extends('layouts.master')

@section('title')
Generic Social Network - Edit Post
@stop

@section('content')
	<div class="timeline">
		
		<div class="title colored">Documentation</div>
		<p>Everything in the assignment task sheet was implemented in full. It was not too difficult as the labs have helped me to learn the specifics of laravel with many relevant examples. I did have to figure out a way to calculate the comment counts for each post in a single SQL query. They are calculated through joining tables and using the SQL count() function.</p>
		<p>I have taken a few interesting approaches in regards to implementing features. Forms are validated first using javascript and the submit button is disabled until the form is valid. Of course they are also validated through PHP to stop people bypassing the JavaScript. All of the styling is my own CSS and I have not used bootstrap. Posts can be edited and deleted from the post page as well as the home page.</p>
		
		<div style="padding-top: 10px; padding-bottom: 10px;" class="title colored">Entity-Relationship Diagram</div>
		<img src="/2503ict-assign1/public/images/erd.svg" alt="Entity-Relationship Diagram" />
		
		<div style="padding-top: 20px;" class="title colored">Site Diagram</div>
		<img src="/2503ict-assign1/public/images/site.svg" alt="Site Diagram" />
		
	</div>
@stop
