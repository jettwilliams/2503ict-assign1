@extends('layouts.master')

@section('title')
Generic Social Network - View Comments
@stop

@section('content')
	<div class="timeline">
		
		<div class="post">
			<div class="user">
				<div class="avatar">
					<img src="/2503ict-assign1/public/images/avatar.svg" width="60" height="60" alt="Avatar" />
				</div>
				<div class="name">
					{{{ $post->name }}}
				</div>
			</div>
			<div class="text">
				<div class="title">
					{{{ $post->title }}}
				</div>
				<div class="message">
					{{{ $post->message }}}
				</div>
				<div class="options">
					<span class="option"><a href="/2503ict-assign1/public/edit/{{{ $post->id }}}">Edit</a></span>
					<span class="option"><a href="/2503ict-assign1/public/action/delete-post/{{{ $post->id }}}">Delete</a></span>
				</div>
			</div>
		</div>
		
		<div class="title colored" style="padding-top: 20px;">Comments</div>
		
		@foreach($comments as $comment)
			<div class="post">
				<div class="user">
					<div class="avatar">
						<img src="/2503ict-assign1/public/images/avatar.svg" width="60" height="60" alt="Avatar" />
					</div>
					<div class="name">
						{{{ $comment->name }}}
					</div>
				</div>
				<div class="text">
					<div class="message">
						{{{ $comment->message }}}
					</div>
					<div class="options">
						<span class="option"><a href="/2503ict-assign1/public/action/delete-comment/{{{ $comment->id }}}">Delete</a></span>
					</div>
				</div>
			</div>
		@endforeach
		
		<div class="title colored" style="padding-top: 20px;">New Comment</div>
		
		<div class="create">
			<form method="post" action="/2503ict-assign1/public/action/insert-comment">
				<input name="postid" type="hidden" value="{{{ $post->id }}}" />
				<input name="user" type="text" placeholder="Name" onkeyup="validateForm()" />
				<textarea name="message" placeholder="Message" rows="4" onkeyup="validateForm()"></textarea>
				<div id="submit" class="button" onclick="submitForm()">Post</div>
			</form>
		</div>
		
	</div>
@stop