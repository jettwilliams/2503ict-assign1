@extends('layouts.master')

@section('title')
Generic Social Network - Edit Post
@stop

@section('content')
	<div class="timeline">
		
		<div class="title colored">Edit Post</div>
		
		<div class="create">
			<form method="post" action="/2503ict-assign1/public/action/edit-post">
				<input name="postid" type="hidden" value="{{{ $post->id }}}" />
				<input name="title" type="text" placeholder="Title" value="{{{ $post->title }}}" onkeyup="validateForm()" />
				<textarea name="message" placeholder="Message" rows="4" onkeyup="validateForm()">{{{ $post->message }}}</textarea>
				<div style="display: table; margin: 0 auto;">
					<div style="display: table-cell; padding-right: 10px;">
						<div id="submit" class="button clickable" onclick="submitForm()">Save</div>
					</div>
					<div style="display: table-cell; padding-left: 10px;">
						<div class="button clickable" onclick="window.history.back()">Cancel</div>
					</div>
				</div>
			</form>
		</div>
		
	</div>
@stop
