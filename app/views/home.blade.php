@extends('layouts.master')

@section('title')
Generic Social Network - Home
@stop

@section('content')
	<div class="timeline">
		
		<div class="title colored">Posts</div>
		
		@foreach($posts as $post)
			<div class="post">
				<div class="user">
					<div class="avatar">
						<img src="/2503ict-assign1/public/images/avatar.svg" width="60" height="60" alt="Avatar" />
					</div>
					<div class="name">
						{{{ $post->name }}}
					</div>
				</div>
				<div class="text">
					<a href="/2503ict-assign1/public/post/{{{ $post->id }}}">
						<div class="title">
							{{{ $post->title }}}
						</div>
					</a>
					<div class="message">
						{{{ $post->message }}}
					</div>
					<div class="options">
						<span class="option"><a href="/2503ict-assign1/public/edit/{{{ $post->id }}}">Edit</a></span>
						<span class="option"><a href="/2503ict-assign1/public/action/delete-post/{{{ $post->id }}}">Delete</a></span>
						<span class="option"><a href="/2503ict-assign1/public/post/{{{ $post->id }}}">View Comments ({{{ $post->numcomments }}})</a></span>
					</div>
				</div>
			</div>
		@endforeach
		
		<div class="title colored" style="padding-top: 20px;">New Post</div>
		
		<div class="create">
			<form method="post" action="/2503ict-assign1/public/action/insert-post">
				<input name="user" type="text" placeholder="Name" onkeyup="validateForm()" />
				<input name="title" type="text" placeholder="Title" onkeyup="validateForm()" />
				<textarea name="message" placeholder="Message" rows="4" onkeyup="validateForm()"></textarea>
				<div id="submit" class="button" onclick="submitForm()">Post</div>
			</form>
		</div>
		
	</div>
@stop