DROP TABLE IF EXISTS posts;

CREATE TABLE posts (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    title TEXT,
    message TEXT
);

/*INSERT INTO posts (name,title,message) VALUES ("Jett","Hello","This is a generic and bad website.");
INSERT INTO posts (name,title,message) VALUES ("Mr. Happy","WHY?","This website is bad and you should feel bad.");
INSERT INTO posts (name,title,message) VALUES ("Tester","Ok","Well at least this junk works.");*/



DROP TABLE IF EXISTS comments;

CREATE TABLE comments (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    message TEXT,
    postid INTEGER
);

/*INSERT INTO comments (name,message,postid) VALUES ("Mr. Happy","You're garbage.",0);
INSERT INTO comments (name,message,postid) VALUES ("Jett","Thanks",0);
INSERT INTO comments (name,message,postid) VALUES ("Tester","This also seems to work ok.",2);*/