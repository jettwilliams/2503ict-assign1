<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//-- Home page
Route::get('/', function()
{
	$posts = getPosts();
	
	return View::make('home')->withPosts($posts);
});

//-- Documentation page
Route::get('docs', function()
{
	return View::make('docs');
});

//-- Post / Comments page
Route::get('post/{id}', function($id)
{
	$posts = getPost($id);
	
	$post = $posts[0]; //-- Get first and only post row
	
	$comments = getComments($id);
	
	return View::make('post')->withPost($post)->withComments($comments);
});

//-- Edit Post page
Route::get('edit/{id}', function($id)
{
	$posts = getPost($id);
	
	$post = $posts[0]; //-- Get first and only post row
	
	return View::make('edit')->withPost($post);
});

//-- Insert post
Route::post('action/insert-post', function()
{
	//-- Get variables through form data
	$name = Input::get('user');
	$title = Input::get('title');
	$message = Input::get('message');
	
	$result = insertPost($name, $title, $message);
	
	if ($result) 
	{
		return Redirect::to(url('.')); //-- Go to home page
	} 
	else
	{
		die("Please fill out all form fields."); //-- If bypass javascript validation
	}
});

//-- Update post
Route::post('action/edit-post', function()
{
	//-- Get variables through form data
	$id = Input::get('postid');
	$title = Input::get('title');
	$message = Input::get('message');
	
	$result = editPost($id, $title, $message);
	
	if ($result) 
	{
		return Redirect::to(url('post/'.$id)); //-- Go to posts / comments page
	} 
	else
	{
		die("Please fill out all form fields."); //-- If bypass javascript validation
	}
});

//-- Delete post
Route::get('action/delete-post/{id}', function($id)
{
	$result = deletePost($id);
	
	if ($result) 
	{
		return Redirect::to(url('.')); //-- Go to home page
	} 
	else
	{
		die("Error deleting post."); //-- If bypass javascript validation
	}
});

//-- Insert comment
Route::post('action/insert-comment', function()
{
	//-- Get variables through form data
	$postid = Input::get('postid');
	$name = Input::get('user');
	$message = Input::get('message');
	
	$result = insertComment($postid, $name, $message);
	
	if ($result) 
	{
		return Redirect::to(url('post/'.$postid)); //-- Go to posts / comments page
	} 
	else
	{
		die("Please fill out all form fields."); //-- If bypass javascript validation
	}
});

//-- Delete comment
Route::get('action/delete-comment/{id}', function($id)
{
	$result = deleteComment($id);
	
	$postid = $result[0]->postid; //-- Get post id from result returned by deleteComment()
	
	if ($postid)
	{
		return Redirect::to(url('post/'.$postid)); //-- Go to posts / comments page
	} 
	else
	{
		die("Error deleting comment."); //-- If bypass javascript validation
	}
});



//-- Get all posts
function getPosts()
{
    //-- Select all posts, order by post id descending, include comment count column
    $query = "SELECT posts.id, posts.name, posts.title, posts.message, COUNT(comments.id) numcomments FROM posts LEFT JOIN comments ON posts.id = comments.postid GROUP BY posts.id ORDER BY posts.id DESC";
    
	$result = DB::select($query);
	
	return $result;
}

//-- Get all comments
function getComments($id)
{
    //-- Select all comments, order by comment id ascending
    $query = "SELECT * FROM comments WHERE postid = ? ORDER BY id ASC";
    
	$result = DB::select($query, array($id));
	
	return $result;
}

//-- Get a single post from a post id
function getPost($id)
{
    $query = "SELECT * FROM posts WHERE id = ?";
    
	$result = DB::select($query, array($id));
	
	return $result;
}

//-- Insert a post by SQL
function insertPost($name, $title, $message) 
{
	$id = 0;
	
	//-- Make sure form input not interpreted as HTML
	$name = htmlspecialchars($name);
	$title = htmlspecialchars($title);
	$message = htmlspecialchars($message);
	
	//-- Make sure form elements not blank
	if ($name != '' && $title != '' && $message != '')
	{
		$query = "INSERT INTO posts (name, title, message) VALUES (?, ?, ?)";
		
		DB::insert($query, array($name, $title, $message));
		
		$id = DB::getPdo()->lastInsertId(); //-- Get id of last inserted row
	}
	
	return $id;
}

//-- Edit a post by SQL
function editPost($id, $title, $message) 
{
	$result = 0;
	
	//-- Make sure form input not interpreted as HTML
	$title = htmlspecialchars($title);
	$message = htmlspecialchars($message);
	
	//-- Make sure form elements not blank
	if ($id != '' && $title != '' && $message != '')
	{
		$query = "UPDATE posts SET title = ?, message = ? WHERE id = ?";
		
		$result = DB::update($query, array($title, $message, $id));
	}
	
	return $result;
}

//-- Delete a post by SQL
function deletePost($id)
{
	$query = "DELETE FROM posts WHERE id=?";

	$result = DB::delete($query, array($id));
	
	//-- Delete comments on the post as well
	$query = "DELETE FROM comments WHERE postid=?";

	DB::delete($query, array($id));
  
	return $result;
}

//-- Insert a comment by SQL
function insertComment($postid, $name, $message) 
{
	$id = 0;
	
	//-- Make sure form input not interpreted as HTML
	$postid = htmlspecialchars($postid);
	$name = htmlspecialchars($name);
	$message = htmlspecialchars($message);
	
	//-- Make sure form elements not blank
	if ($postid != '' && $name != '' && $message != '')
	{
		$query = "INSERT INTO comments (postid, name, message) VALUES (?, ?, ?)";
		
		DB::insert($query, array($postid, $name, $message));
		
		$id = DB::getPdo()->lastInsertId();
	}
	
	return $id;
}

//-- Delete a comment by SQL
function deleteComment($id)
{
	//-- Get post id to redirect to after deleting comment
	$query = "SELECT postid FROM comments WHERE id=?";

	$postid = DB::select($query, array($id));
  	
  	$query = "DELETE FROM comments WHERE id=?";

	$result = DB::delete($query, array($id));
  	
	return $postid;
}